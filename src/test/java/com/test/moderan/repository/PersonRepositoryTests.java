package com.test.moderan.repository;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Example;

import com.test.moderan.model.Person;

@SpringBootTest
public class PersonRepositoryTests {

    @Autowired
    private PersonRepository personRepository;

    @Test
    public void saveTest() {
        Person person = new Person("person");
        personRepository.save(person);
        assert (personRepository.findOne(Example.of(person)).isPresent());
    }

    @Test
    public void searchTest() {
        Person person = new Person("Tom");
        personRepository.save(person);
        Person person2 = new Person("tommy");
        personRepository.save(person2);
        Person person3 = new Person("jack");
        personRepository.save(person3);

        assert (personRepository.findByPersonContainingIgnoreCase("to").size() == 2);
    }
}
