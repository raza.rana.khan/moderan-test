package com.test.moderan.model;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Set;

@SpringBootTest
public class PersonTests {
    Validator validator;

    @Test
    public void whenNotNullName_thenNoConstraintViolations() {
        validator = Validation.buildDefaultValidatorFactory().getValidator();
        Person person = new Person("test");
        Set<ConstraintViolation<Person>> violations = validator.validate(person);

        assert (violations.size() == 0);
    }

    @Test
    public void whenNullName_thenTwoConstraintViolation() {
        validator = Validation.buildDefaultValidatorFactory().getValidator();
        Person person = new Person(null);
        Set<ConstraintViolation<Person>> violations = validator.validate(person);

        assert (violations.size() == 2);
    }

    @Test
    public void whenEmptyName_thenOneConstraintViolations() {
        validator = Validation.buildDefaultValidatorFactory().getValidator();
        Person person = new Person("");
        Set<ConstraintViolation<Person>> violations = validator.validate(person);

        assert (violations.size() == 1);
    }
}