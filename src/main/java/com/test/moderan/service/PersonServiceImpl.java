package com.test.moderan.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.test.moderan.model.Person;
import com.test.moderan.repository.PersonRepository;

import java.util.List;

@Service
@Transactional
public class PersonServiceImpl implements PersonService {

    @Autowired
    private PersonRepository personRepository;

    @Override
    public Person createPerson(Person person) {
        return personRepository.save(person);
    }

    @Override
    public List<Person> searchPerson(String searchedName) {
        return personRepository.findByPersonContainingIgnoreCase(searchedName);
    }
}
