package com.test.moderan.service;

import java.util.List;

import com.test.moderan.model.Person;

public interface PersonService {
    Person createPerson(Person person);

    List<Person> searchPerson(String searchedName);
}
