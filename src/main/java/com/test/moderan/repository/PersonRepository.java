package com.test.moderan.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

import com.test.moderan.model.Person;

public interface PersonRepository extends JpaRepository<Person, Long> {

    List<Person> findByPersonContainingIgnoreCase(String searchedName);
}
