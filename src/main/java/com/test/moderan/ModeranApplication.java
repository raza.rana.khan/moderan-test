package com.test.moderan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ModeranApplication {

    public static void main(String[] args) {
        SpringApplication.run(ModeranApplication.class, args);
    }

}
