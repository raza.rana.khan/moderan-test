package com.test.moderan.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import com.test.moderan.model.Person;
import com.test.moderan.service.PersonService;

@Validated
@RestController
public class PersonController {

    @Autowired
    private PersonService personService;

    @GetMapping("/person")
    public ResponseEntity<List<Person>> searchPerson(@RequestParam("search") String searchedName) {
        return ResponseEntity.ok().body(personService.searchPerson(searchedName));
    }

    @PostMapping("/person")
    public ResponseEntity<Person> createPerson(@RequestBody @Valid Person person) {
        return ResponseEntity.ok().body(this.personService.createPerson(person));
    }
}
